#!/usr/bin/python3

import sys
from docker import Client

EARGS = 1
EUSAG = 2


class ImageNotFound(Exception):
    pass


class MainObj:
    def __init__(self, a):
        super(MainObj, self).__init__()
        self.commands = []
        self.cli = Client(base_url='unix://var/run/docker.sock')
        self.hist = None
        self.docker_key, self.docker_identifier = a[0], a[1]
        self.gen_dockerfile()

    def gen_dockerfile(self):
        self._get_image()
        self.hist = self.cli.history(self.img['RepoTags'][0])
        self._parse_history()
        self.commands.reverse()
        self._print_commands()

    def _print_commands(self):
        for i in self.commands:
            print(i)

    def _get_image(self):
        images = self.cli.images()
        for i in images:
            if self.docker_identifier in i[self.docker_key]:
                self.img = i
                return
        raise ImageNotFound(f"Image {self.docker_identifier} not found\n")

    def _insert_step(self, step):
        if "#(nop)" in step:
            to_add = step.split("#(nop) ")[1]
        else:
            to_add = ("RUN {}".format(step))
        to_add = to_add.replace("&&", "\\\n    &&")
        self.commands.append(to_add.strip(' '))

    def _parse_history(self, rec=False):
        first_tag = False
        actual_tag = False
        for i in self.hist:
            if i['Tags']:
                actual_tag = i['Tags'][0]
                if first_tag and not rec:
                    break
                first_tag = True
            self._insert_step(i['CreatedBy'])
        if not rec:
            self.commands.append("FROM {}".format(actual_tag))


def parse_args():
    if len(sys.argv) < 2 or len(sys.argv) > 3:
        exit_prog(EARGS, "Either one or two arguments needed")
    a, b = str(sys.argv[-2]).lower(), str(sys.argv[-1]).lower()
    if len(sys.argv) == 3 and a not in ['sha', 'repotags']:
        exit_prog(EUSAG, "First position argument: (sha|repotags)")
    if a == 'sha' or len(sys.argv) == 2:
        return ('Id', b)
    if a == 'repotags':
        return ('RepoTags', b)


def exit_prog(n, m):
    print(f"\nError: {m}, exiting...")
    print_help()
    sys.exit(n)


def print_help():
    print("\nUsage:")
    print("  <prog> (sha|repotags) <identifier>")
    print(" or")
    print("  <prog> <sha-identifier>\n")


__main__ = MainObj(parse_args())
